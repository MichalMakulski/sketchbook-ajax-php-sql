<!DOCTYPE html>
<html lang="pl">
	<head>
		<meta charset="utf-8">
		<title>Paint - sketch</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1">
		<script src="js/jquery-1.11.3.min.js"></script>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link href='https://fonts.googleapis.com/css?family=Amatic+SC:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div class="gallery">
			<h2>MY DRAWINGS</h2>
			<div class="drawings-container"></div>		
		</div>
		<span class="show-gallery"><i class="fa fa-picture-o fa-2x"></i></span>
		<span id="clear"><i class="fa fa-pencil-square-o fa-2x"></i></span>
		<span class="msg-saved">Drawing saved!</span>
		<div class="save-container">
			<input id="title" type="text" name="title" placeholder="Drawing title">
			<i class="fa fa-floppy-o fa-2x save"></i>
		</div>	
		<script src="js/canvas.js"></script>
		<script src="js/ajax.js"></script>
		<script src="js/gallery.js"></script>
		<script src="js/app.js"></script>
	</body>
</html>

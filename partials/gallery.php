<?php
	require_once '../db.php';
	$sql = "SELECT * FROM drawings";
	$result = mysqli_query($conn, $sql);

	$rows = array();
	while($r = mysqli_fetch_assoc($result)) {
	    $rows[] = $r;
	}
	echo json_encode($rows);
	mysqli_close($conn);
?>
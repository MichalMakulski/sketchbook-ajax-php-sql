$(document).ready(function(){
	$('form').on('submit', function(ev){
		ev.preventDefault();
		var el = document.querySelector('canvas');
		var dataString = 'title=' + $('#title').val() + '&image=' + el.toDataURL();
		console.log(dataString);
		$.ajax({
			url: 'form.php',
			data: dataString,
			dataType:'text',
			type: 'POST'
		});
	});
});
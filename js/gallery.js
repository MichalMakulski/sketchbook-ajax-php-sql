var GALLERY = (function($){

	var galleryStr = '';

	$('.gallery').prop('visible', false);
		function openGallery(ev){
			if($('.gallery').prop('visible')){
				$('.gallery').css("left", "-300px");
				$('.show-gallery').html('<i class="fa fa-picture-o fa-2x"></i>');
			}else{
				$('.gallery').css("left", "0");
				$('.show-gallery').html('<i class="fa fa-times fa-2x"></i>');
			}
			$('.gallery').prop('visible', !$('.gallery').prop('visible'));
		}

	function loadGallery(response){
		response.map(function(val){
			galleryStr = galleryStr + '<div class="drawing-container" data-id="' + val.id + '">' +
									 '<h2>' + val.title + '</h2>' + 
									 '<span class="remove">REMOVE</span>'+
									 '<img src="' + val.image + '">' +
									 '</div>';
		});
		$('.gallery').html(galleryStr);
	}	
	
	AJAX.getGallery(loadGallery);

	return {
		init: function(){
			$('.show-gallery').bind('click', openGallery);
		}
	}

})(jQuery);


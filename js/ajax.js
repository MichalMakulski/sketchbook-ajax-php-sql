var AJAX = (function($){
	
	var dataString;

	function getGallery(callback){
		$.ajax({
				url: 'partials/gallery.php',
				type: 'POST',
				dataType: 'json',
				success: callback
			});
		}

	function ajaxPost(callback){
		$.ajax({
				url: 'handler.php',
				data: dataString,
				dataType: 'text',
				type: 'POST',
				success: callback
			});
	}

	function drawingSaved(){
		setTimeout(function(){
			$('.msg-saved').fadeIn(700, function(){
    		$(this).fadeOut(700);
  		});
		}, 300);
		getGallery(updateGallery);
	}

	function updateGallery(response){
		var	newDrawing = $('<div class="drawing-container" data-id="' + response[response.length - 1].id + '">' +
									 '<h2>' + response[response.length - 1].title + '</h2>' + 
									 '<span class="remove">REMOVE</span>'+
									 '<img src="' + response[response.length - 1].image + '">' +
									 '</div>');
		$('.gallery').append(newDrawing);
	}	

	function drawingRemoved(ev){
		$(ev.target).parent().fadeOut('slow', function(){
			$(this).remove();
		});
	}

	function handler(ev, action, callback){
		var el = document.querySelector('canvas');
		if(action === 'addDrawing'){
			dataString = 'action=' + action + '&title=' + $('#title').val() + '&image=' + encodeURIComponent(el.toDataURL());
			ajaxPost(callback);

		}	else if (action === 'deleteDrawing'){
			var $drawing = $(ev.target).parent();
			dataString = 'action=' + action + '&id=' + $drawing.data('id');
			ajaxPost(callback);			
		}
	}

	return {
		init: function(){
			$(document).on('click', function(ev){
				var $target = $(ev.target);
				if($target.is($('.save'))){
					handler(ev, 'addDrawing', drawingSaved);
				}else if($target.is($('.remove'))){
					handler(ev, 'deleteDrawing', drawingRemoved(ev)); 
				}
			});
		},
		getGallery: getGallery
	}

})(jQuery);
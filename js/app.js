var APP = (function(){

	return {
		init: function(){
			GALLERY.init();
			AJAX.init();
			CANVAS.init();
		}
	}
	
})();


$(document).ready(APP.init);